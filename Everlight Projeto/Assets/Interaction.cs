﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Interaction : MonoBehaviour
{
    MikeScript player;
    public GameObject dialogue;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<MikeScript>();
        
    }
    void OnTriggerEnter2D(Collider2D col)
    {

        if(col.gameObject.tag == "Player")
        {
            dialogue.SetActive(true);

        }
 
    }
    void OnTriggerExit2D(Collider2D col)
    {

        if(col.gameObject.tag == "Player")
        {
            dialogue.SetActive(false);
           
        }
 
    }

}
