﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    private LineRenderer lineRenderer;
    public Transform laserHit;
    MikeScript player;
    public bool ativado;
    public LayerMask layer;
    AudioSource audio;
  
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        audio = GetComponent<AudioSource>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<MikeScript>();
        lineRenderer.enabled = false;
        ativado = false;

    }

    void Update()
    {
        if(ativado)
        {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, -transform.up, layer);
        Debug.DrawLine(transform.position, hit.point);
        laserHit.position = hit.point;
        lineRenderer.SetPosition(0, transform.position);
        lineRenderer.SetPosition(1, laserHit.position);
        

         if(hit.collider.tag == "Player")
         {
             hit.collider.GetComponent<HealthBar>().DealDamage(10);

             player.animator.SetBool("isDamaged", true);
             StartCoroutine("Return");
            
             player.gameObject.GetComponent<Animation>().Play("RedFlash");
            

                //KNOCKBACK
             player.knockbackCount = player.knockbackLenght;
             if(hit.collider.transform.position.x < transform.position.x)
             {
                 player.knockFromRight = true;
             }
             else
             {
                 player.knockFromRight = false;
             }
         }
        
        }


      
        
    }



    public void LaserON()
    {
        ativado = true;
        lineRenderer.enabled = true;
       
        StartCoroutine("OffLaser");

        
    }

    public void LaserSound()
    {
        audio.Play();
    }


    
    IEnumerator OffLaser()
    {
        
        yield return new WaitForSeconds(5f);
        ativado = false;
        lineRenderer.enabled = false;
        

        
        
    }


    IEnumerator Return()
    {
        yield return new WaitForSeconds(0.2f);
        player.animator.SetBool("isDamaged", false);
        

    }
}
