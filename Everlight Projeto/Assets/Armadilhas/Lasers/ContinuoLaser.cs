﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContinuoLaser : MonoBehaviour
{
    private LineRenderer lineRenderer;
    public Transform laserHit;
    MikeScript player;


  
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<MikeScript>();



    }

    void Update()
    {
      
        RaycastHit2D hit = Physics2D.Raycast(transform.position, -transform.up);
        Debug.DrawLine(transform.position, hit.point);
        laserHit.position = hit.point;
        lineRenderer.SetPosition(0, transform.position);
        lineRenderer.SetPosition(1, laserHit.position);

         if(hit.collider.tag == "Player")
         {
             hit.collider.GetComponent<HealthBar>().DealDamage(10);

             player.animator.SetBool("isDamaged", true);
             StartCoroutine("Return");
            
             player.gameObject.GetComponent<Animation>().Play("RedFlash");
            

                //KNOCKBACK
             player.knockbackCount = player.knockbackLenght;
             if(hit.collider.transform.position.x == transform.position.x)
             {
                 player.knockFromRight = true;
             }
             else
             {
                 player.knockFromRight = false;
             }
         }
        
        


      
        
    }

    IEnumerator Return()
    {
        yield return new WaitForSeconds(0.2f);
        player.animator.SetBool("isDamaged", false);
        

    }


}
