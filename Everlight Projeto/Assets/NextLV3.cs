﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLV3 : MonoBehaviour
{
    MikeScript player;
    public GameObject musica;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<MikeScript>();
        
    }
    void OnTriggerEnter2D(Collider2D col)
    {

        if(col.gameObject.tag == "Player")
        {
            //musica.SetActive(false);
            SceneManager.LoadScene(10);
        }
 
    }
}
