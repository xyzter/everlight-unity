﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Porta2 : MonoBehaviour
{
    Animator animator;
    private MikeScript player;
    public GameObject GO;

    void Start()
    {
        animator = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<MikeScript>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
    
        if(col.gameObject.tag == "Player")
        {
            GO.SetActive(true);
            animator.SetBool("OpenDoor", true);
            animator.SetBool("CloseDoor", false); 

        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if(col.gameObject.tag == "Player")
        {
        animator.SetBool("CloseDoor", true); 
        }
    }
   
}
