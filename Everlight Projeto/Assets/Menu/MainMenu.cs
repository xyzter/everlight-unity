﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MainMenu : MonoBehaviour
{


    public void PlayGame() {

        FindObjectOfType<MenuAudioManager>().Play("Menu botão");
        SceneManager.LoadScene("ICutscene");
        //GetComponent<MenuAudioManager>().enabled = false;
       
    }

    public void QuitGame() {

        FindObjectOfType<MenuAudioManager>().Play("Menu botão");
        Application.Quit();
    }

    public void Credits()
    {
   
        FindObjectOfType<MenuAudioManager>().Play("Menu botão");
 
        SceneManager.LoadScene("Créditos");
    }

    public void Options()
    {
        FindObjectOfType<MenuAudioManager>().Play("Menu botão");
        SceneManager.LoadScene("Options");
    }

    public void Menu()
    {
        FindObjectOfType<MenuAudioManager>().Play("Menu botão");
        SceneManager.LoadScene("Menu");
    }


}
