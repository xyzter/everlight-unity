﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class fim : MonoBehaviour
{

    public string nivel;
    public int tempo;
    public GameObject skip;

    void Start()
    {
        StartCoroutine (waitFor());
        StartCoroutine(Skip());
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene(nivel);
        }
    }

    IEnumerator Skip()
    {
        yield return new WaitForSeconds(0.8f);
        skip.SetActive(false);
    }

    IEnumerator waitFor()
    {
        yield return new WaitForSeconds(tempo);
        SceneManager.LoadScene(nivel);
    }
}
