﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class musicaLv1 : MonoBehaviour
{
    private static musicaLv1 instance;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
