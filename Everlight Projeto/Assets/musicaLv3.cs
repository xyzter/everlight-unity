﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class musicaLv3 : MonoBehaviour
{
    private static musicaLv3 instance;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
