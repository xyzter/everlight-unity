﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : MonoBehaviour
{

    HealthBar playerHealth;

    public float healthBonus = 50f;

    // Start is called before the first frame update
    void Start()
    {
        playerHealth = FindObjectOfType<HealthBar>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(playerHealth.currentHealth < playerHealth.maxHealth)
        {
            //playerHealth.currentHealth = playerHealth.currentHealth + healthBonus;
            playerHealth.DealDamage(-25);
            Destroy(gameObject);
        }
    }


}
