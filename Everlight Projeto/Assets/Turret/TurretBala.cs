﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBala : MonoBehaviour
{
    public float speed;
    public Rigidbody2D rb;
    public float damage = 2.5f;
    MikeScript player;
    
    void Start()
    {
        rb.velocity = transform.right * speed;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<MikeScript>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        HealthBar player = hitInfo.GetComponent<HealthBar>();
        
        if(player != null)
        {
            player.DealDamage(damage);
            player.gameObject.GetComponent<Animation>().Play("RedFlash");
        }



        
        
        DestroyProjectile();
    }

     void DestroyProjectile()
    {

        Destroy(gameObject);
    }

    
    
}
