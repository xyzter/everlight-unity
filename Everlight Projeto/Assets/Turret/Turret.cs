﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    public float startTimeBtwShoots;
    private float timeBtwShoots;
    public GameObject projectile;
    public Transform firepoint;
    AudioSource audio;
    
    void Start()
    {
        timeBtwShoots = startTimeBtwShoots;
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(timeBtwShoots <= 0)
        {
            //Instantiate(projectile, transform.position, Quaternion.identity);
            audio.Play();
            Instantiate(projectile, firepoint.position, firepoint.rotation);
            timeBtwShoots = startTimeBtwShoots;
        }
        else
        {
            timeBtwShoots -= Time.deltaTime;
        }
    }
}
