﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{

    public float speed;
    public Rigidbody2D rb;
    public int damage = 5;
    public float lifeTime;

    public GameObject impactEffect;

    MikeScript player;
  

    

    
    void Start()
    {

        rb.velocity = transform.right * speed;

        player = FindObjectOfType<MikeScript>();

        Invoke("DestroyProjectile", lifeTime);
    }


    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        Enemy enemy = hitInfo.GetComponent<Enemy>();
        
        if(enemy != null)
        {
            enemy.DealDamage(damage);
        }

        
        EnemyShield enemyS = hitInfo.GetComponent<EnemyShield>();
        
        if(enemyS != null)
        {
            enemyS.DealDamage(damage);
        }

      

        //Instantiate(impactEffect, transform.position, transform.rotation); 
        
        DestroyProjectile();
    }

    void DestroyProjectile()
    {
        //Instantiate(impactEffect, transform.position, Quaternion.identity); 
        

        Destroy(gameObject);
    }

}
