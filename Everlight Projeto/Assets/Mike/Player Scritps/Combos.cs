﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Combos
{
    public Hit[] hits;

    
}

[Serializable]
public class Hit
{
    public string animator;
    public string inputButton;
    public float animatorTime;
    public float resetTime;

    public int damage;
    //public AudioClip hitSound;
}
