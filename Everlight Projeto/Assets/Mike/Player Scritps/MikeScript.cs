﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MikeScript : MonoBehaviour
{
    //movimento
    [Header ("Variaveis de Movimento")]
    public float speed;
	private float moveInput;

	public Rigidbody2D rb;
	public bool facingRight = true; 
    

 
//-------------------------------------
    //PULO
    [Header ("Variaveis de Pulo")]
    public float jumpForce;
    private bool isGrounded;

    public Transform groundCheck;
	public float checkRadius;
	public LayerMask whatIsGround;

    private float jumpTimeCounter;
	public float jumpTime;
	private bool isJumping;
    private bool doubleJump;

    //private bool canJump;
//----------------------------------
    //Dash	
    [Header ("Variaveis do Dash")]
	public float dashSpeed;
	private float dashTime;
	public float startDashTime;
	private int direction;

    private bool slide;
//----------------------------------------

    //ATAQUE MELEE
    [Header ("Variáveis ATK COMBO")]
    public Combos[] combos;
    
    private bool startCombo;
    
    public List<string> currentCombo;
    
    private float comboTimer;

    private Hit currentHit, nextHit;

    private bool canHit = true;

    private bool resetCombo;

    public AtKtrigger attack;
//----------------------------------------
    [Header ("Variaveis do Tiro")]
    public Transform firePoint;
    public GameObject bulletPrefab;
    
    private float timeBtwShot;
    public float startBtwShot;

    private bool shoot;

    private bool shooting;
//----------------------------------------
    [Header ("Variaveis do Wall Jump/Slide")]
    //Wall Slide/Jump


    public float wallSlideForce;
    public bool wallSliding;

    
    public Transform wallCheck;
    private bool walled = false;
    public float wallJumpForce;
    public LayerMask whatIsWall;
//-----------------------------------------
    [Header ("Outras Variáveis")]
    public Animator animator;

    private bool crouching;
    
    public bool ComAChave;
    public GameObject KeyIcon;
    
    //private bool crouch = false;
    //public float cellingRadius;
    //public Transform cellingCheck;

    [Header ("Knockback")]
    public float knockbackX;
    public float knockbackY;
    public float knockbackLenght;
    public float knockbackCount;
    public bool knockFromRight;


    HealthBar health;
//----------------------------------------
    
    
    void Start()
    {   
        dashTime = startDashTime;

        animator = GetComponent<Animator>();
        
        rb = GetComponent<Rigidbody2D>();

        ComAChave = false;
        
        health = GetComponent<HealthBar>();

    }

    void Update()
    {
        CheckInputs();
        


    }

    void CheckInputs()
    {
//-----------------------------------------------------PULO------------------------------------------\\
       

        if(isGrounded == true && Input.GetKeyDown(KeyCode.Space))
		{
            //canJump = true;
            animator.SetTrigger("takeOff");
			isJumping = true;
			jumpTimeCounter = jumpTime;
			rb.velocity = Vector2.up * jumpForce;
		}

        if (isGrounded == true)
        {
            doubleJump = false;
            animator.SetBool("isJumping", false);
        }
        else
        {
            animator.SetBool("isJumping", true);
        }

        if(Input.GetKey(KeyCode.Space) && isJumping == true)
		{
            
			if (jumpTimeCounter > 0)
			{
                //canJump = true;
				rb.velocity = Vector2.up * jumpForce;
				jumpTimeCounter -= Time.deltaTime;
                
			}
			else 
			{
				isJumping = false;
			}
		}

        if(Input.GetKeyUp(KeyCode.Space))
        {
            isJumping = false;
        }

        if(isGrounded == false && doubleJump == false && Input.GetKeyDown(KeyCode.Space))
        {

            //canJump = true; 
            isJumping = true;
            doubleJump = true;
            isJumping = true;
            jumpTimeCounter = jumpTime;
            rb.velocity = Vector2.up * jumpForce;
            animator.SetTrigger("takeOff");
            FindObjectOfType<AudioManager>().Play("Mike Pulando");
        }

//--------------------------------------------------ATK-----------------------------------------\\

        


        

        if(Input.GetButtonDown("Fire1") && !canHit)
        {
            resetCombo = true;
        }

        for(int i = 0; i < combos.Length; i++)
        {
            if(combos[i].hits.Length > currentCombo.Count)
            {

            if(Input.GetButtonDown(combos[i].hits[currentCombo.Count].inputButton)&& isGrounded)
            {
                
                if(currentCombo.Count == 0)
                {
                    PlayHit(combos[i].hits[currentCombo.Count]);
                    break;
                }
                else
                {
                    bool comboMatch = false;
                    for(int y = 0; y < currentCombo.Count; y++)
                    {
                        if(currentCombo[y] != combos[i].hits[y].inputButton)
                        {
                            comboMatch = false;
                            break;
                        }
                        else
                        {
                            comboMatch = true;
                        }

                    }

                    if(comboMatch && canHit)
                    {
                        
                        nextHit = combos[i].hits[currentCombo.Count];
                        canHit = false; 
                        break;
                    }



                }
            }
         
            }
        }

        if(startCombo)
        {
            comboTimer += Time.deltaTime;

            if(comboTimer >= currentHit.animatorTime && !canHit)
            {
                PlayHit(nextHit);

                if(resetCombo)
                {
                    canHit = false;
                    CancelInvoke();
                    Invoke("ResetCombo", currentHit.animatorTime);
                }
            }

            if(comboTimer >= currentHit.resetTime)
            {
                ResetCombo();
            }

            if(canHit)
            {
                shoot = false;
                rb.velocity = Vector2.zero;
                slide = false;
               
                
            }
            
            
        }







//--------------------------------------------------------------------------------------------------\\
    }

    void PlayHit(Hit hit)
    {
        comboTimer = 0;
        attack.SetAttack(hit);
        animator.Play(hit.animator);
        startCombo = true;
        currentCombo.Add(hit.inputButton);
        currentHit = hit;

        canHit = true;

    
    }

      void ResetCombo()
    {
        startCombo = false;
        comboTimer = 0;
        currentCombo.Clear();
        animator.Rebind();

        canHit = true;
        
    }
//------------------------------------------------------------------------------------------------\\

    void FixedUpdate()
    {
    
        
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);
        walled = Physics2D.Linecast(transform.position, wallCheck.position, whatIsWall);
        //crouch = Physics2D.OverlapCircle(cellingCheck.position, cellingRadius, whatIsGround);

       //SCRIPT PULO\\ 
        

//------------------------------------MOVIMENTOS--------------------------------------------------\\    
		moveInput = Input.GetAxisRaw("Horizontal");
        //rb.velocity = new Vector2 (moveInput * speed, rb.velocity.y);
        //animator.SetFloat("Speed",Mathf.Abs(moveInput));

        //KNOCKBACK
        if(knockbackCount <= 0)
        {
            HandleMovement();
        }
        /*else
        {
            if(knockFromRight)
            {
                rb.velocity = new Vector2 (knockbackX, -knockbackY);
            }
             if(!knockFromRight)
            {
                rb.velocity = new Vector2 (-knockbackX, knockbackY);
                knockbackCount -= Time.deltaTime;
            }
           
        }*/
        else
        {
            if(knockFromRight)
            {
                rb.velocity = new Vector2 (knockbackX, knockbackY);
                 knockbackCount -= Time.deltaTime;
            }
             if(!knockFromRight)
            {
                rb.velocity = new Vector2 (-knockbackX, knockbackY);
                knockbackCount -= Time.deltaTime;
            }
           
        }

        animator.SetFloat("Speed",Mathf.Abs(moveInput));

        
		if(( moveInput > 0 && !facingRight) || (moveInput < 0 && facingRight))
		{
          
          //Wall JUMP
            if(walled && !isGrounded)
            {
                //animator.SetBool("WallJump", true);
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y);
                rb.AddForce(new Vector2(0f, wallJumpForce));
                

            }
            

            
            Flip();
        }


        
        //WALL SLIDE
        if(!isGrounded)
        {
            

            if(facingRight && Input.GetAxisRaw("Horizontal") > 0f || 
            !facingRight && Input.GetAxisRaw("Horizontal") < 0f)
            {

                if(walled && !isGrounded)
                {
                    animator.SetBool("isWallSliding", true);
                    HandleWallSliding();

                
                }    
            }
        }
            if(walled == false || isGrounded)
            {
                wallSliding = false;
                animator.SetBool("isWallSliding", false);
            }


//-------------------------------------------TIRO----------------------------------------\\
        
            if(Input.GetKeyDown(KeyCode.C) && isGrounded && timeBtwShot <= 0) 
            {
                
                Shoot();
                slide = false;
                shoot = true;
                //animator.Play("Mike Atirando");
                timeBtwShot = startBtwShot;

                
                
            }
   
            else
            {
                shoot = false;
                timeBtwShot -= Time.deltaTime;
            }

            if (shoot)
            {
            //animator.SetTrigger("Tiro");
            rb.velocity = Vector2.zero;
            animator.Play("Mike Atirando");
            }




        //-------------------------------------------------------------------------------------


        //---------------------------------------------DASH------------------------------------------\\

        if (direction == 0)
		{   
            
        
			if (Input.GetKeyDown(KeyCode.X) && !facingRight)
			{
                slide = true;
				direction = 1;
                
			}
           
			if (Input.GetKeyDown(KeyCode.X) && facingRight)
			{
                slide = true;
				direction = 2;
			}
          
            
        }
    
        else
		{
				if (dashTime <= 0 && isGrounded)
				{
                    
					direction = 0;
					dashTime = startDashTime;
					rb.velocity = Vector2.zero;
                    animator.SetBool ("isDashing", false);
                    gameObject.GetComponent<CapsuleCollider2D>().enabled = true;
                                        
				}
				else
				{
                    
					dashTime -= Time.deltaTime;
                    shoot = false;
                    //animator.SetBool ("isDashing", true);
                    
                    
                    
                    

                
                    if(slide)
                    {
                        
					    if(direction == 1 && isGrounded)
					    {
                        animator.SetBool ("isDashing", true);
						rb.velocity = Vector2.left * dashSpeed;
                        
                        
                        }

					    if(direction == 2 && isGrounded)
					    {
						animator.SetBool ("isDashing", true);
                        rb.velocity = Vector2.right * dashSpeed;
                        
                        
					    }
            
                    }
                  
                    
                }

                    
                    
				

        }
//----------------------------------------AGACHAR---------------------------------------------\\

        if(crouching)
        {
            animator.SetFloat("Speed", 0);
            animator.SetBool("isCrouching", true);
            
            speed = 0f;
            
            /*if(Physics2D.OverlapCircle(cellingCheck.position, cellingRadius, whatIsGround))
            {
                crouching = true;
            }*/
        }
        if(Input.GetKey(KeyCode.DownArrow) && isGrounded)
        {
            
            slide = false;
            crouching = true;

            
        }

        else
        {
            crouching = false;
            animator.SetFloat("Speed",Mathf.Abs(moveInput));
            animator.SetBool("isCrouching", false);
            
            speed = 10f;
        }

//---------------------------------------------------------------------------------




    
    
    
    
    
    
    
    

    
    
    
    }
//------------------------------------------------VOIDs----------------------------------------------------------//

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "KeyCard")
        {
            ComAChave = true;
            KeyIcon.SetActive(true);
            Destroy(col.gameObject);

        }

        if(col.gameObject.CompareTag("Plataforma Móvel"))
        {
            
            this.transform.parent = col.gameObject.transform;
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
         if(col.gameObject.CompareTag("Plataforma Móvel"))
        {
            transform.parent = null;
        }
    }

    void HandleMovement()
    {
        if(!this.animator.GetCurrentAnimatorStateInfo(0).IsTag("Dash") && 
        !this.animator.GetCurrentAnimatorStateInfo(0).IsTag("Attacking") && 
        !this.animator.GetCurrentAnimatorStateInfo(0).IsTag("Shot"))
        {
		rb.velocity = new Vector2 (moveInput * speed, rb.velocity.y);
        }
    }

    private void Flip()
	{
        
		facingRight = !facingRight;
		transform.Rotate(0f, 180f, 0f);
	
        /*Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;*/
    
    
    }


    void Shoot()
    {
        
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        FindObjectOfType<AudioManager>().Play("Tiro Mike");
    }
    

    //--------------------------------------------------------------------------------
     



    void HandleWallSliding()
    {
        rb.velocity = new Vector2(rb.velocity.x, -wallSlideForce);
        wallSliding = true;
        

        
    }
    //--------------------------------------------------------------------------------

    public void slideSound()
    {
        FindObjectOfType<AudioManager>().Play("Mike Slide");
    }

    public void Steps()
    {
        FindObjectOfType<AudioManager>().Play("Mike Passos");
    }

    public void ATK()
    {
        FindObjectOfType<AudioManager>().Play("Mike ATK");
    }
    public void Dano()
    {
        FindObjectOfType<AudioManager>().Play("Mike Dano");
    }

    public void Morte()
    {
        FindObjectOfType<AudioManager>().Play("Mike Morrendo");
    }

}
