﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtKtrigger : MonoBehaviour
{

	private int damage;

	//EnemyShield enemyS;
	//InimigoParado enemyP;


	void Awake()
    {
       //enemyS = GameObject.FindGameObjectWithTag("Enemy Shield").GetComponent<EnemyShield>();
	   //enemyP = GameObject.FindGameObjectWithTag("EnemyParado").GetComponent<InimigoParado>();
	
    }

	public void SetAttack(Hit hit)
	{
		damage = hit.damage;
		
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		Enemy enemy = other.GetComponent<Enemy>();
		
		
		if(enemy != null)
		{
			enemy.DealDamage(damage);
		
		}

		EnemyShield enemyS = other.GetComponent<EnemyShield>();
		
		if(enemyS != null)
		{
			enemyS.DealDamage(damage);
		
		}

		InimigoParado enemyP = other.GetComponent<InimigoParado>();
		
		if(enemyP != null)
		{
			enemyP.DealDamage(damage);
		
		}


		// if(other.gameObject.tag == "Enemy Shield")
        // {
        //     enemyS.DealDamage(damage);
        // }

		// if(other.gameObject.tag == "EnemyParado")
        // {
        //     enemyP.DealDamage(damage);
        // }
	
	}
}
