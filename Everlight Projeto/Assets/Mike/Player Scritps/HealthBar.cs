﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HealthBar : MonoBehaviour
{
    public Slider healthBar;

    public float currentHealth;
    public float maxHealth = 100f;
    
    MikeScript player;

  

    
    void Awake()
    {

             
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<MikeScript>();

        currentHealth = maxHealth;
        //healthBar.value = CalculateHealth();
        healthBar.value = maxHealth;
    }

    void Update()
    {
    
    }

    //CALCULAR O DANO
    public void DealDamage(float damageValue)
    {
        currentHealth -= damageValue;
        //healthBar.value = CalculateHealth();
        healthBar.value = currentHealth;
        
        if(currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
        

        //MATAR O JOGADOR
        if(currentHealth <= 0)
        {
            player.animator.SetBool("isDead", true);
            GetComponent<MikeScript>().enabled = false;
            player.gameObject.GetComponent<MikeScript>().rb.velocity = Vector2.zero;
        
            
            StartCoroutine("RestartScene");

            //Application.LoadLevel(Application.loadedLevel);
            //Destroy(gameObject);
        }
    }

    IEnumerator RestartScene()
    {
        yield return new WaitForSeconds(2.0f);
        Scene scene;
        scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);

    }
    /*float CalculateHealth()
    {
        return currentHealth / maxHealth;
    }*/

      void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Enemy Melee ATK")
        {   
             //Causar Dano
        DealDamage(5);

        player.animator.SetBool("isDamaged", true);
        StartCoroutine("Return");
            
        player.gameObject.GetComponent<Animation>().Play("RedFlash");
            

        ///KNOCKBACK
        player.knockbackCount = player.knockbackLenght;
        if(col.transform.position.x < transform.position.x)
        {
        player.knockFromRight = true;
        }
        else
        {
             player.knockFromRight = false;
        }
         }
     //---------------------------------------\\
     if(col.gameObject.tag == "Enemy Shield ATK")
     {   
         //Causar Dano
        DealDamage(10);

           player.animator.SetBool("isDamaged", true);
           StartCoroutine("Return");
            
         player.gameObject.GetComponent<Animation>().Play("RedFlash");
            

            //KNOCKBACK
          player.knockbackCount = player.knockbackLenght;
         if(col.transform.position.x < transform.position.x)
          {
             player.knockFromRight = true;
         }
         else
        {
             player.knockFromRight = false;
        }
         }

        if(col.gameObject.tag == "Choque")
         {
       player.knockbackCount = player.knockbackLenght;
      if(col.transform.position.x < transform.position.x)
          {
              player.knockFromRight = true;
           }
           else
           {
                player.knockFromRight = false;
         }
            
        StartCoroutine("RestartScene2");
         DealDamage(1000);
         player.animator.SetBool("isDead", false);
          player.animator.SetBool("isChoque", true);
            
         GetComponent<MikeScript>().enabled = false;
         player.gameObject.GetComponent<MikeScript>().rb.velocity = Vector2.zero;
        
        }
        
    }

    IEnumerator Return()
    {
        yield return new WaitForSeconds(0.2f);
        player.animator.SetBool("isDamaged", false);
        

    }

    IEnumerator RestartScene2()
    {
        yield return new WaitForSeconds(0.8f);
        Destroy(gameObject);
        Scene scene;
        scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
        

    }



}
