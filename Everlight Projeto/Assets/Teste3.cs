﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teste3 : MonoBehaviour
{
    public DialogBase dialogue;

    public void TriggerDialogue()
    {
        DialogManager2.instance.EnqueueDialogue(dialogue);

    }

    private void Start()
    {
        TriggerDialogue();
    }
}
