﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Dia : MonoBehaviour
{
    public Animator animator;
    private int levelToLoad;


   

    public static Dia instance;
    private void Awake()
    {


        animator = GetComponent<Animator>();
        
        if(instance != null)
        {
            Debug.LogWarning("fix this" + gameObject.name);
        }
        else
        {
            instance = this;
        }
    }

    public GameObject dialogueBox;
    public Text dialogueName;
    public Text dialogueText;
    public Image dialoguePortrait;
    public float delay = 0.002f;
    
    private bool isCurrentlyTyping;
    private string completeText;
    public bool diaboxfalse;

    public Queue<DialogBase.Info> dialogueInfo = new Queue<DialogBase.Info>();
//---------------------------------
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Y))
        {
            FadeToNextLevel();
        }
        if(diaboxfalse)
        {
            dialogueBox.SetActive(false);
        }
    }
    
    public void FadeToNextLevel()
    {
        FadeToLevel(SceneManager.GetActiveScene().buildIndex + 1);
    }
    
    public void FadeToLevel(int levelIndex)
    {
        levelToLoad = levelIndex;
        animator.SetTrigger("FadeOut");
    }

    public void OnFadeComplete()
    {

        SceneManager.LoadScene(levelToLoad);
    }
//-------------------
    public void EnqueueDialogue(DialogBase db)
    {
        dialogueBox.SetActive(true);
        dialogueInfo.Clear();

        foreach(DialogBase.Info info in db.dialogueInfo)
        {
            dialogueInfo.Enqueue(info);

        }
        DequeueDialogue();
    }

    public void DequeueDialogue()
    {

        if(dialogueInfo.Count == 0)
        {
            EndDialogue();
            return;
        }

        if(isCurrentlyTyping == true )
        {
            CompleteText();
            StopAllCoroutines();
            isCurrentlyTyping = false;
            return;
        }
        
        DialogBase.Info info = dialogueInfo.Dequeue();
        
        completeText = info.text;

        //dialogueName.text = info.name;
        dialogueName.text = info.character.name;
        dialogueText.text = info.text;
        //dialoguePortrait.sprite = info.portrait;
        dialoguePortrait.sprite = info.character.portrait;
        
        dialogueText.text = "";

        StartCoroutine(TypeText(info));
    }

    IEnumerator TypeText(DialogBase.Info info)
    {
        isCurrentlyTyping = true;
        //dialogueText.text = "";
        foreach(char c in info.text.ToCharArray())
        {
            yield return new WaitForSeconds(delay);
            dialogueText.text += c;
            //yield return null;
        }
        isCurrentlyTyping = false;
    }

    private void CompleteText()
    {
        dialogueText.text = completeText;
    }

    public void EndDialogue()
    {
        //dialogueBox.SetActive(false);
        diaboxfalse = true;
        //animator.Play("Molly Mochila");
        //GetComponent<LevelChanger>().FadeToNextLevel();
        StartCoroutine("NextScene");

    }

    IEnumerator NextScene()
    {
        yield return new WaitForSeconds(1.0f);
        FadeToNextLevel();
        //SceneManager.LoadScene(0);
    }


}
