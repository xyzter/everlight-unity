﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Dialogue", menuName = "Dialogues")]
public class DialogBase: ScriptableObject
{

    [System.Serializable]
    public class Info
    {

    public CharacterProfile character;
    

    //public string name;
    //public Sprite portrait;
    [TextArea(4, 8)]
    public string text;
    }

    [Header("Insert Dialogue")]
    public Info[] dialogueInfo;
    
}
