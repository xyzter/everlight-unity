﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teste2 : MonoBehaviour
{
    public DialogBase dialogue;

    public void TriggerDialogue()
    {
        DialogManager.instance.EnqueueDialogue(dialogue);

    }

    /*private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            TriggerDialogue();
        }
    }*/

    private void Start()
    {
        TriggerDialogue();
    }
}
