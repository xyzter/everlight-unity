﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasterEgg : MonoBehaviour
{
    public GameObject interrogacao;

    void OnTriggerEnter2D(Collider2D col)
    {
        MikeScript player = col.GetComponent<MikeScript>();
        if(player != null)
        {
            interrogacao.SetActive(true);
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        MikeScript player = col.GetComponent<MikeScript>();
        if(player != null)
        {
            interrogacao.SetActive(false);
        }
    }
}
