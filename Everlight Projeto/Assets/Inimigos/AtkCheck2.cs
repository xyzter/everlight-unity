﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtkCheck2 : MonoBehaviour
{
      MikeScript player;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<MikeScript>();
        
    }
    void OnTriggerEnter2D(Collider2D col)
    {

        if(col.gameObject.tag == "Player")
        {
            InimigoParado.isAttacking = true;
        }
 
    }


    void OnTriggerExit2D(Collider2D col)
    {
        if(col.gameObject.tag == "Player")
        {
            InimigoParado.isAttacking = false;

        }
    }
}
