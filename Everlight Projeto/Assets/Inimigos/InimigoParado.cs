﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InimigoParado : MonoBehaviour
{
    
    //public Animator animator;
    public int healthEnemy;

    public float speedFollow;
    public bool moveRight;
    public float followDistance;

    public float wallCheckRadius;
    public Transform WallCheck;
    public LayerMask whatIsWall;
    public Transform EdgeCheck;
    public Transform target;
    private bool hitWall;
    private bool notAtEgde;
    public bool isChasing;
    public bool movingIn = false;

    public static bool isAttacking = false;

    

    Animator animator;
    Rigidbody2D rb;
    MikeScript player;



    void Start()
    {
        target = GameObject.Find("Player").GetComponent <Transform>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<MikeScript>();

        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();


    }

 
 

    void FixedUpdate()
    {

    hitWall = Physics2D.OverlapCircle(WallCheck.position, wallCheckRadius, whatIsWall);
    notAtEgde =  Physics2D.OverlapCircle(EdgeCheck.position, wallCheckRadius, whatIsWall);
    
 
        if(hitWall || !notAtEgde)
        {
            moveRight = !moveRight;
                  
        }




//-----------------------------------------------------------------------
        float stoppingDistance = Vector2.Distance(transform.position, target.position); 
           
            if(Vector2.Distance(transform.position, target.position) < followDistance  && stoppingDistance > 1)
            {   
                
                isChasing = true;
                movingIn = true;
                animator.SetBool("Walking", true);

                
                
                if(target.position.x > transform.position.x && !moveRight) 
                {
                    Flip();
                    transform.eulerAngles = new Vector3(0, -180, 0);
                }

                else if(target.position.x < transform.position.x && moveRight)
                {
                    Flip();
                    transform.eulerAngles = new Vector3(0, 0, 0);
                }
                
                if(movingIn)
                {
                transform.position = Vector2.MoveTowards(transform.position, 
                new Vector2 (target.position.x, transform.position.y), speedFollow * Time.deltaTime);
                }
                if(isChasing && !notAtEgde || hitWall)
                {
                    isChasing = false;
                    speedFollow = 0;
                    animator.SetBool("Walking", false);
                    movingIn = false;

                }
               
              
            
            }
            else
            {
                isChasing = false;
                movingIn = false;
                animator.SetBool("Walking", false);
            }

             

     
        if(healthEnemy <= 0)
        {
            GetComponent<InimigoParado>().enabled = false;
            rb.velocity = Vector2.zero;
            isAttacking = false;
            animator.SetBool("isDead", true);
            
        
        }

        if(isAttacking)
        {
            
            isChasing = false;
            rb.velocity = Vector2.zero;
            animator.SetBool("isAttacking", true);
            
            speedFollow = 0;
            
        }
        else
        {
            animator.SetBool("isAttacking", false);
        
        }

        if(!this.animator.GetCurrentAnimatorStateInfo(0).IsTag("Attack") && !hitWall && notAtEgde)
        {
            
            speedFollow = 5;
        }

        



    }
    
//------------------------------------------VOIDS------------------------------\\
    void Flip()
    {
      //sprite.flipX = !sprite.flipX;
      moveRight = !moveRight;

    }
    
    void OnTriggerEnter2D(Collider2D col)
    {
    

        if(col.gameObject.tag == "Bullet")
        {   
            DealDamage(5);
        }
    }




    public void DealDamage(int damage)
    {
        healthEnemy -= damage;
        gameObject.GetComponent<Animation>().Play("RedFlash"); 
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position,followDistance);
    }

    public void Morte()
    {
        FindObjectOfType<AudioManager>().Play("Inimigo Morrendo");
    }

    public void ATK()
    {
        FindObjectOfType<AudioManager>().Play("Inimigo ATK");
    }


}
