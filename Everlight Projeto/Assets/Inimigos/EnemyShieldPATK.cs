﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShieldPATK : MonoBehaviour
{
      MikeScript player;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<MikeScript>();
        
    }
    void OnTriggerEnter2D(Collider2D col)
    {

        if(col.gameObject.tag == "Player")
        {
            EnemyShieldP.isAttacking = true;
        }
 
    }


    void OnTriggerExit2D(Collider2D col)
    {
        if(col.gameObject.tag == "Player")
        {
            EnemyShieldP.isAttacking = false;

        }
    }
}
