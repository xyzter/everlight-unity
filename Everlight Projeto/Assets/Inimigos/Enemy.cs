﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    
    //public Animator animator;
    public int healthEnemy;

    public float speed;
    public float speedFollow;
    public bool moveRight;
    public float followDistance;

    public float wallCheckRadius;
    public Transform WallCheck;
    public LayerMask whatIsWall;
    public Transform EdgeCheck;
    public Transform target;
    private bool hitWall;
    private bool notAtEgde;
    public bool isPatrol;
    public bool isChasing;
    public bool movingIn = false;

    public static bool isAttacking = false;
    //public float distanceAtk;
    //SpriteRenderer sprite;
    

    Animator animator;
    Rigidbody2D rb;
    MikeScript player;

    /*float PlayerDistance()
    {
        return Vector2.Distance(target.position, transform.position);
    }*/

    void Start()
    {
        target = GameObject.Find("Player").GetComponent <Transform>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<MikeScript>();

        //sprite = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();

        isPatrol = true;
    }

 
 

    void FixedUpdate()
    {

    hitWall = Physics2D.OverlapCircle(WallCheck.position, wallCheckRadius, whatIsWall);
    notAtEgde =  Physics2D.OverlapCircle(EdgeCheck.position, wallCheckRadius, whatIsWall);
    
    if(isPatrol)
    {
        isChasing = false;

        if(hitWall || !notAtEgde)
        {
            moveRight = !moveRight;
            //Flip();           
        }

        if(moveRight)
        {
            transform.eulerAngles = new Vector3(0, -180, 0);
            //transform.localScale = new Vector2(-1f, 1f);
            rb.velocity = new Vector2(speed, rb.velocity.y);
            animator.SetBool("Walking", true);
        }
        else
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
            //transform.localScale = new Vector2(1f, 1f);
            rb.velocity = new Vector2(-speed, rb.velocity.y);
            animator.SetBool("Walking", true);
        }
    }
    else
    {
        //isChasing = true;
        isPatrol = false;
    }


//-----------------------------------------------------------------------
        float stoppingDistance = Vector2.Distance(transform.position, target.position); 
           
            if(Vector2.Distance(transform.position, target.position) < followDistance  && stoppingDistance > 1)
            {   
                
                isChasing = true;
                isPatrol = false;
                movingIn = true;
                animator.SetBool("Walking", true);

                /*if(( target.position.x > transform.position.x && !sprite.flipX) || 
                (target.position.x < transform.position.x && sprite.flipX))
                {
                    Flip();
                }*/
                
                if(target.position.x > transform.position.x && !moveRight) //|| (target.position.x < transform.position.x && moveRight))
                {
                    Flip();
                    transform.eulerAngles = new Vector3(0, -180, 0);
                }

                else if(target.position.x < transform.position.x && moveRight)
                {
                    Flip();
                    transform.eulerAngles = new Vector3(0, 0, 0);
                }
                
                transform.position = Vector2.MoveTowards(transform.position, 
                new Vector2 (target.position.x, transform.position.y), speedFollow * Time.deltaTime);
                
                 if(isChasing && !notAtEgde || hitWall)
                {
                    isChasing = false;
                    //speedFollow = 0;
                    animator.SetBool("Walking", false);
                    movingIn = false;
                    isPatrol = true;

                }
              
            
            }
            else
            {
                isChasing = false;
                isPatrol = true;
            }

        //VIDA
        if(healthEnemy <= 0)
        {
            GetComponent<Enemy>().enabled = false;
            rb.velocity = Vector2.zero;
            isAttacking = false;
            animator.SetBool("isDead", true);
            //StartCoroutine("Destroy");
        
        }

        if(isAttacking)
        {
            isPatrol = false;
            isChasing = false;
            rb.velocity = Vector2.zero;
            animator.SetBool("isAttacking", true);
            speed = 0;
            speedFollow = 0;
            
        }
        else
        {
            animator.SetBool("isAttacking", false);
        
        }

        if(!this.animator.GetCurrentAnimatorStateInfo(0).IsTag("Attack") && !hitWall && notAtEgde)
        {
            speed = 5;
            speedFollow = 5;
        }

        



    }
    
//------------------------------------------VOIDS------------------------------\\
    void Flip()
    {
      //sprite.flipX = !sprite.flipX;
      moveRight = !moveRight;
      //transform.eulerAngles = new Vector3(0, -180, 0);
    }
    
    void OnTriggerEnter2D(Collider2D col)
    {
        /*HealthBar health = col.GetComponent<HealthBar>();
        
        
        if(health != null)
        {   
            //Causar Dano
            health.DealDamage(5);

            player.animator.SetBool("isDamaged", true);
            StartCoroutine("Return");
            
            player.gameObject.GetComponent<Animation>().Play("RedFlash");
            

            //KNOCKBACK
            player.knockbackCount = player.knockbackLenght;
            if(col.transform.position.x == transform.position.x)
            {
                player.knockFromRight = true;
            }
            else
            {
                player.knockFromRight = false;
            }
        }*/

        if(col.gameObject.tag == "Bullet")
        {   
            DealDamage(5);
        }
    }

    /*IEnumerator Return()
    {
        yield return new WaitForSeconds(0.2f);
        player.animator.SetBool("isDamaged", false);
        

    }*/

    /*IEnumerator Destroy()
    {
        yield return new WaitForSeconds(2.0f);
        Destroy(gameObject);
    }*/



    public void DealDamage(int damage)
    {
        healthEnemy -= damage;
        gameObject.GetComponent<Animation>().Play("RedFlash"); 
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position,followDistance);
    }

    public void Morte()
    {
        FindObjectOfType<AudioManager>().Play("Inimigo Morrendo");
    }

    public void ATK()
    {
        FindObjectOfType<AudioManager>().Play("Inimigo ATK");
    }

}
