﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLvCutscene : MonoBehaviour
{
    MikeScript player;
    //public GameObject Musica;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<MikeScript>();
        
    }
    void OnTriggerEnter2D(Collider2D col)
    {

        if(col.gameObject.tag == "Player")
        {
            //Musica.SetActive(false);
            SceneManager.LoadScene(12);
        }
 
    }
}
