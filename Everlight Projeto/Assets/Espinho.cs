﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Espinho : MonoBehaviour
{   
    MikeScript player;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<MikeScript>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
    HealthBar health = col.GetComponent<HealthBar>();
        
        
         if(health != null)
         {   
            //Causar Dano
             health.DealDamage(5);

             player.animator.SetBool("isDamaged", true);
             StartCoroutine("Return");
            
             player.gameObject.GetComponent<Animation>().Play("RedFlash");
            

                //KNOCKBACK
             player.knockbackCount = player.knockbackLenght;
             if(col.transform.position.x > transform.position.x)
             {
                 player.knockFromRight = true;
             }
             else
             {
                 player.knockFromRight = false;
             }
         }
    }

    IEnumerator Return()
    {
        yield return new WaitForSeconds(0.2f);
        player.animator.SetBool("isDamaged", false);
        

    }
}
