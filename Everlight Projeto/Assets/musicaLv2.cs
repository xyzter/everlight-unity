﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class musicaLv2 : MonoBehaviour
{
    private static musicaLv2 instance;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
