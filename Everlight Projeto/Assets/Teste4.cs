﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teste4 : MonoBehaviour
{
public DialogBase dialogue;

    public void TriggerDialogue()
    {
        DialogManager3.instance.EnqueueDialogue(dialogue);

    }

    private void Start()
    {
        TriggerDialogue();
    }
}
