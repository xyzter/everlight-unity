﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortaScript : MonoBehaviour
{
    Animator animator;
    private MikeScript player;
    public GameObject portaInteracao;
    public GameObject GO;

    void Start()
    {
        animator = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<MikeScript>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Player" && !player.ComAChave)
        {   
            portaInteracao.SetActive(true); 
    
        }
        if(col.gameObject.tag == "Player" && player.ComAChave)
        {
            GO.SetActive(true);
            animator.SetBool("OpenDoor", true);
            player.KeyIcon.SetActive(false);

        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if(col.gameObject.tag == "Player")
        {
        portaInteracao.SetActive(false); 
        }
    }
   
}
