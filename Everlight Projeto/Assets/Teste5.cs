﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teste5 : MonoBehaviour
{
    public DialogBase dialogue;

    public void TriggerDialogue()
    {
        DialogManager4.instance.EnqueueDialogue(dialogue);

    }

    private void Start()
    {
        TriggerDialogue();
    }
}
