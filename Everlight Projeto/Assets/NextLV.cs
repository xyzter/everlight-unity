﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLV : MonoBehaviour
{
    MikeScript player;
    public GameObject audiomanager;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<MikeScript>();
        
    }
    void OnTriggerEnter2D(Collider2D col)
    {

        if(col.gameObject.tag == "Player")
        {   
            audiomanager.SetActive(false);
            SceneManager.LoadScene(6);
        }
 
    }
}
