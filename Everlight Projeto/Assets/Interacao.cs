﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Interacao : MonoBehaviour
{
    MikeScriptSemTiro player;
    public GameObject Interac;
    //public GameObject musica;
    public bool change;

    void Awake()
    {
        change = false;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<MikeScriptSemTiro>();
        
    }
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.G) && change)
        {
            //musica.SetActive(false);
            SceneManager.LoadScene("Cutscene");
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {

        if(col.gameObject.tag == "Player")
        {
            Interac.SetActive(true);
            change = true;
        }
        
 
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if(col.gameObject.tag == "Player")
        {
            Interac.SetActive(false);

        }
    }
}
